
/* ----------------------------------------
 * Video CE settings
 */
tt_content.videoce_videocontent.20 {
    settings {
        default {
            extLinkIcon = {$plugin.tx_videoce.video.default.extLinkIcon}
            width = 300
            height = 150
        }

        videoTypes {
            youtube {
                class = Simplicity\Videoce\Domain\Model\YoutubeVideo

                config {
                    pattern = #^(?:https?://|//)?(?:www\.|m\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/watch\?.+&v=))([\w-]{11})#x

                    # normal link youtube website
                    link {
                        url = https://www.youtube.com/watch?v=
                        urlParam =
                    }
                    # embed config
                    embed {
                        # url for embedding the video
                        url = //www.youtube.com/embed/
                        #url = //www.youtube-nocookie.com/embed/
                        # params for embed url
                        urlParam = ?rel=0
                        # with subtitles (in specific lang)
                        #urlParam = ?rel=0&cc_lang_pref=de&cc_load_policy=1
                        urlParamPlaylist = &playlist=
                        # iframe attribs
                        iframeAttrib = frameborder="0" allowfullscreen
                    }
                    # show in lightbox
                    lightbox {
                        enabled = 1
                        url = https://www.youtube.com/watch?v=
                        urlParam = &width=720&height=400
                    }
                }
            }

            vimeo {
                class = Simplicity\Videoce\Domain\Model\VimeoVideo

                config {
                    pattern = #^(?:https?://)?(?:www\.)?(?:vimeo\.com)#x

                    respectAspectRatio = height
                    api {
                        url = http://vimeo.com/api/oembed.json?url=
                    }

                    # normal link vimeo website
                    link {
                        url = https://vimeo.com/
                        urlParam =
                    }
                    # embed config
                    embed {
                        # url for embedding the video
                        url = //player.vimeo.com/video/
                        # see http://developer.vimeo.com/player/embedding
                        urlParam = ?portrait=0&byline=0
                        urlParamPlaylist =
                        # iframe attribs
                        iframeAttrib = frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen
                    }
                    # show in lightbox
                    lightbox {
                        enabled = 1
                        url = https://vimeo.com/
                        urlParam = &width=720&height=400
                    }
                }
            }

            dailymotion {
    
                class = Simplicity\Videoce\Domain\Model\DailymotionVideo

                config {
                    pattern = #^(?:https?://)?(?:www\.)?(?:dailymotion\.com/video/)#x
                  
                    api {
                        url =  https://api.dailymotion.com/video/
                    }

                    # normal link dailymotion website
                    link {
                        url = http://www.dailymotion.com/video/
                        urlParam =
                    }

                    # embed config
                    embed {
                        # url for embedding the video
                        url = http://www.dailymotion.com/embed/video/
                        
                        # iframe attribs
                        iframeAttrib = frameborder="0"
                    }
                    # show in lightbox
                    lightbox {
                        enabled = 1
                        url = http://www.dailymotion.com/embed/video/
                        urlParam = ?autoplay=true&iframe=true
                    }
                }
            }

        }

        rowClass = row
        colClasses {
            1 = col-md-12
            2 = col-md-6
            3 = col-md-4 col-sm-6
            4 = col-md-3 col-sm-6
            6 = col-md-2 col-sm-4
        }

        clickEnlargeATagRel = prettyPhoto

        jsFiles {
            jQueryFitvid = typo3conf/ext/videoce/Resources/Public/Js/jquery.fitvid.js
            videoce = typo3conf/ext/videoce/Resources/Public/Js/videoce.js
        }
        jsIncludeAsFooterFile = 0

    }
}
